/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MorseElements_H
#define MorseElements_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"
#include <stdint.h>
#include <stdbool.h>
#include "ES_Events.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitMorseElements, CalWaitForRise, CalWaitForFall,
              EOC_WaitRise, EOC_WaitFall, DecodeWaitRise, DecodeWaitFall} MorseState_t;

// Public Function Prototypes

//bool InitLCDService(uint8_t Priority);
//bool PostLCDService(ES_Event_t ThisEvent);
//ES_Event_t RunLCDService(ES_Event_t ThisEvent);
               
//Functions
bool InitializeMorseElements(uint8_t Priority) ;
bool PostMorseElement(ES_Event_t ThisEvent) ;
bool CheckMorseEvents(void) ;
ES_Event_t RunMorseElements(ES_Event_t ThisEvent) ;

#endif 
