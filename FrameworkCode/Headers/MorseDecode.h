/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef DecodeMorse_H
#define DecodeMorse_H

#include <stdint.h>
#include <stdbool.h>
#include "ES_Events.h"

bool InitMorseDecode(uint8_t Priority);
bool PostMorseDecode(ES_Event_t ThisEvent);

ES_Event_t RunMorseDecode (ES_Event_t ThisEvent);

#endif /* LCDService_H */


#ifndef DecodeMorseService_H
#define DecodeMorseService_H

#include <stdint.h>
#include <stdbool.h>
#include "ES_Events.h"

// Public Function Prototypes
bool InitializeMorseDecode(uint8_t Priority);
bool PostMorseDecode(ES_Event_t ThisEvent);
ES_Event_t RunMorseDecode (ES_Event_t ThisEvent);

#endif /* ServTemplate_H */
