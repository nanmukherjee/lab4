/*Morse Code Sample Pseudo Code Using the Gen2.x Event Framework
Rev 14 10/21/15
*************************************************************************************
Pseudo-code for the Decode Morse module (a simple service)
Data private to the module: MorseString, the arrays LegalChars and MorseCode
*/

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

//Tiva Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/* include header files for the other modules in Lab3 that are referenced
 */
//#include "LCD_Write.h"
#include "LCDService.h"
//#include "MorseElements.h"
#include "MorseDecode.h"
#include "BITDEFS.H"

#define ALL_BITS (0xff << 2)

static uint8_t  MyPriority;
static int      maxlength = 36;
static char     MorseString[36]; //(maxlength);
// static bool reset;
// static uint8_t StringPosition = 0;
static char legalChars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890?.,:'-/()\"= ";
static char morseCode[][8] = { ".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
                               "....", "..", ".---", "-.-", ".-..", "--", "-.", "---",
                               ".--.", "--.-", ".-.", "...", "-", "..-", "...-",
                               ".--", "-..-", "-.--", "--..", ".----", "..---",
                               "...--", "....-", ".....", "-....", "--...", "---..",
                               "----.", "-----", "..--..", ".-.-.-", "--..--",
                               "---...", ".----.", "-....-", "-..-.", "-.--.-",
                               "-.--.-", ".-..-.", "-...-" };
//static ES_Event_t ReturnValue.EventType = ES_NO_EVENT;
//static char Message;

//Prototypes
char DecodeMorseString(void);

//**************************InitMorseDecode*************************
bool InitMorseDecode(uint8_t Priority)
{
  bool ReturnVal;

//Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;
  MorseString[0] = '\0';
  //MorseString = "\0";
  //strcpy(MorseString,"\0");
  //reset = 1;
  ReturnVal = 1;
  //return 1;
  return ReturnVal;
}

//************PostMorseDecode****************************/

bool PostMorseDecode(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

//************RunMorseDecode****************************/

ES_Event_t RunMorseDecode(ES_Event_t ThisEvent)
{
  ES_Event_t  NoEventReturn;
  ES_Event_t  Event2Post;
  char        return_char;
  NoEventReturn.EventType = ES_NO_EVENT;
  //puts ("in morse decode service\n");
  //ES_Event_t Reset;
  //ES_Event_t Event2Post;
  //Event2Post.EventType = ES_LCD_PUTCHAR;

  if (ThisEvent.EventType == ES_DOT_DETECTED)
  {
    if (strlen(MorseString) < maxlength)
    {
      strcat(MorseString, ".");
      //puts("strcat .");
    }
    else
    {
      NoEventReturn.EventType = ES_ERROR;
      NoEventReturn.EventParam = strlen(MorseString);
      //PostMorseDecode(ThisEvent);
    }
  }

  if (ThisEvent.EventType == ES_DASH_DETECTED)
  {
    if (strlen(MorseString) < maxlength)
    {
      strcat(MorseString, "-");
      //puts("strcat -");
    }
    else
    {
      NoEventReturn.EventType = ES_ERROR;
      NoEventReturn.EventParam = strlen(MorseString);
    }
  }

  if (ThisEvent.EventType == ES_EOC_DETECTED)
  {
    //if (reset == 0) {
    //puts("entered EOC");
    return_char = DecodeMorseString();
    Event2Post.EventType = ES_LCD_PUTCHAR;
    Event2Post.EventParam = return_char;
    //Event2Post.EventParam  = DecodeMorseString();

    PostLCDService(Event2Post);
    //}
    //LCDWriteCommand8(Message);
    //strcpy(MorseString,"\0");
    MorseString[0] = '\0';
    //StringPosition = 0;
  }

  if (ThisEvent.EventType == ES_EOW_DETECTED)
  {
    //if (reset == 0) {
    return_char = DecodeMorseString();
    Event2Post.EventType = ES_LCD_PUTCHAR;
    Event2Post.EventParam = return_char;
    PostLCDService(Event2Post);
    //}
    //reset = 0;
    return_char = ' ';
    Event2Post.EventParam = return_char;
    PostLCDService(Event2Post);

    //Message = DecodeMorseString(MorseString);
    //PostLCDService(Message);
    //PostLCDService(); //print space
    //strcpy(MorseString,"\0");
    //*MorseString = "\0";
    //StringPosition = 0;
    MorseString[0] = '\0';
  }

  if (ThisEvent.EventType == ES_BAD_SPACE)
  {
    //strcpy(MorseString,"\0");
    //*MorseString = "\0";
    //StringPosition = 0;
    MorseString[0] = '\0';
  }
  if (ThisEvent.EventType == ES_BAD_PULSE)
  {
    //strcpy(MorseString,"\0");
    //*MorseString = "\0";
    //StringPosition = 0;
    MorseString[0] = '\0';
  }
  if (ThisEvent.EventType == ES_DB_BUTTON_DOWN)
  {
    //Event2Post.EventParam = '~';
    //PostLCDService(Event2Post);

    //strcpy(MorseString,"\0");
    //*MorseString = "\0";
    //StringPosition = 0;
    MorseString[0] = '\0';
    //reset = 1;
  }
  return NoEventReturn;
}

//************DecodeMorseString****************************/

char DecodeMorseString(void)
{
  int   i;
  char  return_char;
  int   compval;

  printf("MorseString %s \r\n", MorseString);

  for (i = 0; i < sizeof(legalChars); i++)     //MorseString size > legalChars
  {
    compval = strcmp(MorseString, morseCode[i]);

    if (compval == 0)
    {
      return_char = legalChars[i];
      printf("Returning character %c \r\n", return_char);
      return return_char;
    }
  }
  //else{  // don't want to return ~ at every wrong character,
  return_char = '~';
  return return_char;
  //}
  //}
  //return return_char;
}
