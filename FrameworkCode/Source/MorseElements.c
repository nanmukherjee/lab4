/*Morse Code Sample Pseudo Code Using the Gen2.x Event Framework
Rev 14 10/21/15
*************************************************************************************
Pseudo-code for the Morse Elements module (a service that implements a state machine)
Data private to the module: MyPriority, CurrentState, TimeOfLastRise, TimeOfLastFall, LengthOfDot, FirstDelta, LastInputState
*/

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// tiva library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
//#include "ES_DeferRecall.h"
//#include "ES_ShortTimer.h"

/* include header files for the other modules in Lab3 that are referenced
*/
#include "MorseDecode.h"
//#include "LCDService.h"
#include "MorseElements.h"

//#define ALL_BITS (0xff<<2)
//#define PORTB HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS))
//#define PIN3 BIT3HI

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t      MyPriority;
static MorseState_t CurrentState = InitMorseElements;
static uint16_t     TimeOfLastRise;
static uint16_t     TimeOfLastFall;
static uint16_t     LengthOfDot;
static uint16_t     FirstDelta;
static uint8_t      LastInputState;

/*---------------------------Prototypes for Functions----------------------*/
void TestCalibration(void);
void CharacterizeSpace(void);
void CharacterizePulse(void);

/*-----------------Functions-InitializeMorseElements----------------*/

bool InitializeMorseElements(uint8_t Priority)
{
//	printf("initialize\n");
//Takes a priority number, returns True.
  ES_Event_t ThisEvent;
//Initialize the MyPriority variable with the passed in parameter.
  MyPriority = Priority;
//Initialize the port line to receive Morse code
//SR_Init();

  //TERMIO_Init();
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; //enable port B (bit 1)
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1)
  {}
  // set up port B by enabling the peripheral clock, waiting for the

  //HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT3HI);
  // peripheral to be ready and setting the direction
  //HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (BIT3LO); // | SCLK_HI | RCLK_HI);
  // of PB3 to input

  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT3HI);
  // enable
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (BIT3LO);
  //set as input 0
  //HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT3LO);
//Sample port line and use it to initialize the LastInputState variable
  LastInputState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT3HI);
//Set CurrentState to be InitMorseElements
  CurrentState = InitMorseElements;
//Set FirstDelta to 0
  FirstDelta = 0;
//Post Event ES_Init to MorseElements queue (this service)
  ThisEvent.EventType = ES_INIT;
//End of InitializeMorseElements

  if (ES_PostToService(MyPriority, ThisEvent) == 1)
  {
    //printf("initialize posted\n");
    return 1;
  }
  else
  {
    return 0;
    //printf("initialize not posted");
  }
}

/*----------------------Functions PostMorseElement-----------------------*/
bool PostMorseElement(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/*----------------------Functions CheckMorseEvents-----------------------*/

bool CheckMorseEvents(void)
{
//Takes no parameters, returns True if an event was posted (11/4/11 jec)
  ES_Event_t  ThisEvent;
  bool        RetrunVal;
  RetrunVal = 0;
  //puts("in check morse elements");
  // LastInputState = HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) & (BIT3HI); //should be static?
//Get the CurrentInputState from the input line
  uint8_t CurrentInputState;
  CurrentInputState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT3HI);
  //printf("LastInputState: %d CurrentInputState: %d \r\n", LastInputState, CurrentInputState);
//If the state of the Morse input line has changed
  if (CurrentInputState != LastInputState)
  {
    //puts("New input state");
    //If the current state of the input line is high
    if (CurrentInputState == BIT3HI)
    {
      //PostEvent RisingEdge with parameter of the Current Time
      ThisEvent.EventType = ES_RISING_EDGE;
      ThisEvent.EventParam = ES_Timer_GetTime();
      //printf(" GetTime %d", ThisEvent.EventParam);
      ES_PostToService(MyPriority, ThisEvent);
      //puts("Post Rising Edge \n");
    }
    else  //(current input state is low)
    {     //PostEvent FallingEdge with parameter of the Current Time
      ThisEvent.EventType = ES_FALLING_EDGE;
      ThisEvent.EventParam = ES_Timer_GetTime();
      ES_PostToService(MyPriority, ThisEvent);
      //puts("Post Falling Edge \n");
    } //Endif
    RetrunVal = 1;
  }
//Endif
//Set LastInputState to CurrentInputState
  LastInputState = CurrentInputState;
//Return ReturnVal
  //printf("Morse check %d\n", RetrunVal);
  return RetrunVal;
}//End of CheckMorseEvents

/*----------------------Functions RunMorseElements------------------*/

ES_Event_t RunMorseElements(ES_Event_t ThisEvent)
{
  MorseState_t  NextState;
  NextState = CurrentState;
  ES_Event_t    NoEventReturn;
  //puts("in Run Function \r\n");
  switch (CurrentState)
  {
    case InitMorseElements:
    {
      //puts(" in InitMorseElements \n");
      if (ThisEvent.EventType == ES_INIT)
      {
        //puts("ES_INIT \n");
        NextState = CalWaitForRise;
      }
    }
    break;
    case CalWaitForRise:
    {
      //puts("in CalWaitForRise \n");
      if (ThisEvent.EventType == ES_RISING_EDGE)
      {
        //puts("ES_RISING_EDGE \n");
        //Set TimeOfLastRise to Time from event parameter
        TimeOfLastRise = ThisEvent.EventParam;
        NextState = CalWaitForFall;
      }
      if (ThisEvent.EventType == ES_CALIBRATION_COMPLETE)
      {
        //puts("ES_CALIBRATION_COMPLETE \n");
        NextState = EOC_WaitRise;
      }
    }
    break;
    case CalWaitForFall:
    {
      //puts("CalWaitForFall \n");
      if (ThisEvent.EventType == ES_FALLING_EDGE)
      {
        //puts("ES_FALLING_EDGE \n");
        //Set TimeOfLastRise to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
        NextState = CalWaitForRise;
        TestCalibration();
      }
    }
    break;
    case EOC_WaitRise:
    {
      puts("EOC_WaitRise \n");
      if (ThisEvent.EventType == ES_RISING_EDGE)
      {
        puts("Rising Edge detected");
        //Set TimeOfLastRise to Time from event parameter
        TimeOfLastRise = ThisEvent.EventParam;
        NextState = EOC_WaitFall;
        CharacterizeSpace();
      }
      if (ThisEvent.EventType == ES_DB_BUTTON_DOWN)
      {
        NextState = CalWaitForRise;
        FirstDelta = 0;
      }
    }
    break;
    case EOC_WaitFall:
    {
      puts("EOC_WaitFall \n");
      if (ThisEvent.EventType == ES_FALLING_EDGE)
      {
        //Set TimeOfLastRise to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
        NextState = EOC_WaitRise;
      }
      if (ThisEvent.EventType == ES_DB_BUTTON_DOWN)
      {
        NextState = CalWaitForRise;
        FirstDelta = 0;
      }
      if (ThisEvent.EventType == ES_EOC_DETECTED)
      {
        NextState = DecodeWaitFall;
      }
    }
    break;
    case DecodeWaitRise:
    {
      //puts("DecodeWaitRise \n");
      if (ThisEvent.EventType == ES_RISING_EDGE)
      {
        //Set TimeOfLastRise to Time from event parameter
        TimeOfLastRise = ThisEvent.EventParam;
        NextState = DecodeWaitFall;
        CharacterizeSpace();
      }
      if (ThisEvent.EventType == ES_DB_BUTTON_DOWN)
      {
        NextState = CalWaitForRise;
        FirstDelta = 0;
      }
    }
    break;
    case DecodeWaitFall:
    {
      //puts("DecodeWaitFall \n");
      if (ThisEvent.EventType == ES_FALLING_EDGE)
      {
        //Set TimeOfLastRise to Time from event parameter
        TimeOfLastFall = ThisEvent.EventParam;
        NextState = DecodeWaitRise;
        CharacterizePulse();
      }
      if (ThisEvent.EventType == ES_DB_BUTTON_DOWN)
      {
        NextState = CalWaitForRise;
        FirstDelta = 0;
      }
    }
    break;
  }
  CurrentState = NextState;
  //puts("exiting morse run\n");
  NoEventReturn.EventType = ES_NO_EVENT;
  //ThisEvent.EventType = ES_NO_EVENT;
  return NoEventReturn;
}

/*--------------------Test Calibration------------------*/

void TestCalibration()
{
  uint16_t    SecondDelta;
  ES_Event_t  ThisEvent;
  if (FirstDelta == 0)
  {
    FirstDelta = TimeOfLastFall - TimeOfLastRise; // Fall > Rise
    //printf("Time of Fall %d ,  Time of Rise %d \n", TimeOfLastFall, TimeOfLastRise);
    //printf("Last: %d  First: %d  First Delta %d\n", TimeOfLastFall, TimeOfLastRise, FirstDelta);
  }
  else
  {
    SecondDelta = TimeOfLastFall - TimeOfLastRise;
    //printf("Last: %d  First: %d  Second Delta %d\n", TimeOfLastFall, TimeOfLastRise, SecondDelta);
    if ((100.0 * FirstDelta / SecondDelta) <= 33.33)
    {
      LengthOfDot = FirstDelta;
      ThisEvent.EventType = ES_CALIBRATION_COMPLETE;
      //ThisEvent.EventParam = ES_Timer_GetTime();
      PostMorseElement(ThisEvent);
    }
    else if ((100.0 * FirstDelta / SecondDelta) > 300.0)
    {
      LengthOfDot = SecondDelta;
      ThisEvent.EventType = ES_CALIBRATION_COMPLETE;
      //ThisEvent.EventParam = ES_Timer_GetTime();
      PostMorseElement(ThisEvent);
    }
    else
    {
      FirstDelta = SecondDelta;
    }
  }
  //return;
}

/*--------------------CharacterizeSpace------------------*/

void CharacterizeSpace(void)
{
  uint16_t    LastInterval;
  uint16_t    epsilon = 3;
  ES_Event_t  Event2Post;

  LastInterval = TimeOfLastRise - TimeOfLastFall; //Which order?
  //printf( "Fall: %d Rise: %d LastInterval  %d \n",TimeOfLastFall, TimeOfLastRise, LastInterval);
  //printf( "Dot = %d\n",         LengthOfDot);
  //LastInterval not OK for a Dot Space
  //if ((LastInterval <= (LengthOfDot - epsilon)) | (LastInterval >= (LengthOfDot + epsilon)))
  if ((LastInterval <= (LengthOfDot - epsilon)) | (LastInterval >= (LengthOfDot + epsilon)))
  {
    //PostEvent EOCDetected Event to Decode Morse Service & Morse Elements Service
    if ((LastInterval >= (LengthOfDot - epsilon) * 3) & (LastInterval <= (LengthOfDot + epsilon) * 3))
    {
      //char space multiphy by 3, word space multiply by 7
      Event2Post.EventType = ES_EOC_DETECTED;
      puts("  eoc ");
      //puts("characterize space ES_EOC_DETECTED");
      PostMorseElement(Event2Post);
      PostMorseDecode(Event2Post);
      //length of dot should be ~170
      //PostMorseDecode(Event2Post); //Where is this decode morse service defined?
    }
    else
    {
      if ((LastInterval >= (LengthOfDot - epsilon) * 7) & (LastInterval <= (LengthOfDot + epsilon) * 7))
      {
        Event2Post.EventType = ES_EOW_DETECTED;  //needs to be defined
        //PostMorseElement(ThisEvent);
        //puts("characterize space ES_EOW_DETECTED");
        puts("     eow    \r\n");
        PostMorseElement(Event2Post);
        PostMorseDecode(Event2Post);
      }
      else
      {
        Event2Post.EventType = ES_BAD_SPACE;
        PostMorseElement(Event2Post);
        //PostMorseDecode (Event2Post);
      }
    }
  }
  //return;
}

/*--------------------CharacterizePulse------------------*/

void CharacterizePulse(void)
{
  uint16_t    LastPulseWidth;
  uint8_t     epsilon = 3;
  ES_Event_t  Event2Post;

  LastPulseWidth = TimeOfLastFall - TimeOfLastRise;

  // printf( "LastPulseWidth  %d\n", LastPulseWidth);
  // printf( "Dot = %d\n",         LengthOfDot);

  //LastPulseWidth OK for a Dot Space
  if ((LastPulseWidth >= (LengthOfDot - epsilon)) & (LastPulseWidth <= (LengthOfDot + epsilon)))
  {
    Event2Post.EventType = ES_DOT_DETECTED;     //needs to be defined
    puts(".");
    //PostMorseElement(ThisEvent);
    //puts("pulsewidth ES_DOT_DETECTED");
    PostMorseDecode(Event2Post);
  }
  else
  {
    if ((LastPulseWidth >= (LengthOfDot - epsilon) * 3) & (LastPulseWidth <= (LengthOfDot + epsilon) * 3))
    {
      //char space multiphy by 3, word space multiply by 7
      Event2Post.EventType = ES_DASH_DETECTED;
      puts("-");
      PostMorseDecode(Event2Post); //Where is this decode morse service defined?
    }
    else
    {
      Event2Post.EventType = ES_BAD_PULSE;
      PostMorseDecode(Event2Post);
    }
  }
  //return;
}
