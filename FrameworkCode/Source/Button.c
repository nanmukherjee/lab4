/*Morse Code Sample Pseudo Code Using the Gen2.x Event Framework
Rev 14 10/21/15
*************************************************************************************
Pseudo-code for the Button module (a service t
Data private to the module: LastButtonState
*/
#include <stdint.h>
#include <stdbool.h>
#include <termio.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/*TivaLibrary*/
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/* include header files for the other modules in Lab3 that are referenced
*/
//#include "LCD_Write.h"
//#include "LCDService.h"
#include "MorseElements.h"
#include "Button.h"
#include "BITDEFS.H"

//#define ALL_BITS (0xff<<2)

//static variables
static uint8_t        LastButtonState; //ButtonState_t LastButtonState?
static ButtonState_t  CurrentState;
static uint8_t        CurrentButtonState;
static uint8_t        MyPriority;

//************InitializeButtonDebounce****************************/
bool InitializeButtonDebounce(uint8_t Priority)
{
  MyPriority = Priority;
  bool ReturnVal;
  ReturnVal = 0;

  //enable port B (bit 1) with time delay
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;
  while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1)
  {}
  // portB digital input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT4HI);

  // make Port B an input
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= (BIT4LO);

  // pin 4 low
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= (BIT4LO);

  LastButtonState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT4HI);

  CurrentButtonState = Debouncing;
  // Initializes and starts debounce timer
  ES_Timer_InitTimer(BUTTON_TIMER, 100);

  //return 1;
  ReturnVal = 1;
  return ReturnVal;
}

//************PostInitializeButtonDebounce****************************/

bool PostButton(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

//************CheckButtonEvents****************************/

bool CheckButtonEvents(void)
{
  ES_Event_t  ThisEvent;
  //uint8_t CurrentButtonState;
  bool        ReturnVal = 0;
  CurrentButtonState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & (BIT4HI);

  if (CurrentButtonState != LastButtonState)
  {
    if (CurrentButtonState == 0)
    {
      puts("ES_BUTTON_DOWN \n");
      ThisEvent.EventType = ES_BUTTON_DOWN;
      PostButton(ThisEvent);
    }
    else         // (CurrentButtonState & BIT4HI)
    {
      ThisEvent.EventType = ES_BUTTON_UP;
      PostButton(ThisEvent);
      puts("ES_BUTTON_UP \n");
    }
    ReturnVal = 1;
  }
  LastButtonState = CurrentButtonState;
  return ReturnVal;
}

//************RunButtonDebounce****************************/
ES_Event_t RunButtonDebounce(ES_Event_t ThisEvent)
{
  ES_Event_t NoEventReturn;
  //NoEventReturn.EventType = ES_NO_EVENT;
  //ES_Event_t ThisEvent;
  //puts("in button service\n");
  switch (CurrentState)
  {
    case Debouncing:            // If current state is state one
    { puts("inDebouncing \n");
      { if ((ThisEvent.EventType == ES_TIMEOUT) & (ThisEvent.EventParam == BUTTON_TIMER))
        {
          CurrentState = Ready2Sample;
        }
      } }
      break;
    case Ready2Sample:
    {
      if (ThisEvent.EventType == ES_BUTTON_UP)
      {
        //puts("in ES_BUTTON_UP \n");
        ES_Timer_InitTimer(BUTTON_TIMER, 100);
        ThisEvent.EventType = ES_DB_BUTTON_UP;
        CurrentState = Debouncing;
        PostMorseElement(ThisEvent);
        PostButton(ThisEvent);
      }
      else if (ThisEvent.EventType == ES_BUTTON_DOWN)
      {
        //puts("in ES_BUTTON_Down \n");
        ES_Timer_InitTimer(BUTTON_TIMER, 100);
        CurrentState = Debouncing;
        ThisEvent.EventType = ES_DB_BUTTON_DOWN;
        PostMorseElement(ThisEvent);
        PostButton(ThisEvent);
      }
    }
    break;
  }
  puts("in end button debounce \n");
  //PostMorseElement(ThisEvent);
  //PostButton(ThisEvent);
  NoEventReturn.EventType = ES_NO_EVENT;
  return NoEventReturn;
}
