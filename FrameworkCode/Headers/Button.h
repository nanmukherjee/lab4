/****************************************************************************

  Header file for Test Harness Service0
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef Button_H
#define Button_H
#include <stdint.h>
#include <stdbool.h>
#include "ES_Events.h"
#include "ES_Configure.h"
#include "ES_Types.h"

// Button States
typedef enum {Debouncing, Ready2Sample} ButtonState_t;

// Functions
bool InitializeButtonDebounce(uint8_t Priority);
bool CheckButtonEvents(void);
bool PostButton(ES_Event_t ThisEvent);
ES_Event_t RunButtonDebounce(ES_Event_t ThisEvent);

#endif

